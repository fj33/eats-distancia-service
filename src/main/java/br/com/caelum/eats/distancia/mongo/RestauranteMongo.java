package br.com.caelum.eats.distancia.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "restaurantes")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestauranteMongo {

    @Id
    private Long id;

    private String cep;

    private Long tipoDeCozinhaId;
}
